import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.toMap;

public class Main {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();

        Scanner reader = new Scanner(System.in);
        HashMap<String, Integer> wordsInText = new HashMap<>();
        String[] blacklistArray = {"you", "i", "we", "and", "the", "of", "to", "that", "in", "a"}; //etc
        Set blacklist = new HashSet(Arrays.asList(blacklistArray));

        // File reader
        File file = createFile(args, reader);
        try {
            reader = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("'" + file + "'" + " bestaat niet.");
            System.exit(9);
        }

        // bekijk elke vers
        while (reader.hasNextLine()) {
            inspectVerse(reader, wordsInText, blacklist);
        }
        reader.close();

        long endTime = System.currentTimeMillis();
        printStatistics(wordsInText, startTime, endTime);
    }

    public static File createFile(String[] args, Scanner reader) {
        File file = null;
        String filename;

        if (args.length == 0) {
            System.out.print("Please enter the filename: ");
            filename = reader.nextLine();
        } else {
            filename = args[0];
        }

        file = new File(filename);

        return file;
    }

    public static void inspectVerse(Scanner reader, HashMap<String, Integer> wordsInText, Set<String> blacklist) {
        String verse = reader.nextLine();
        String[] wordsInVerse = verse.split(" ");

        for (String word : wordsInVerse) {
            // maak elk woord lowercase
            word = word.toLowerCase();

            // woorden schoonmaken
            if (word.endsWith("'s")) {
                word = word.replaceAll("'s", "");
            }

            word = word.replaceAll("\\W", "");
            word = word.replaceAll("[0-9]", "");

            // toevoegen aan de hashmap, en als het woord al bestaat wordt deze opgehoogd
            if (!blacklist.contains(word)) {
                wordsInText.put(word, wordsInText.getOrDefault(word, 0) + 1);
            }
        }
    }

    public static void printStatistics(HashMap<String, Integer> wordsInText, long startTime, long endTime) {
        System.out.println("The HashMap contains " + wordsInText.size() + " words.");
        System.out.println("God is mentioned " + wordsInText.get("god") + " times.");
        sortMap(wordsInText);
        System.out.println("It took " + (endTime - startTime) + " milliseconds");
    }

    public static void sortMap(HashMap<String, Integer> wordsInText) {
        Map<String, Integer> sorted = wordsInText
                .entrySet()
                .stream()
                .sorted(comparingByValue())
                .collect(
                        toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                                LinkedHashMap::new));

        sorted = wordsInText
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));

        System.out.println("map after sorting by values in descending order: "
                + sorted.);
    }
}